<?php defined('BASEPATH') OR exit ('No direct script access allowed');

class M_Location extends CI_Model
{

	public function getKecamatan()
	{
        return $this->db->query('select * from tb_kecamatan')->result();
    }
    
    public function getDesa()
	{
        return $this->db->query('select * from tb_desa')->result();
    }
    
    public function getDesaByKecamatan()
	{   
        $id_kecamatan = $_POST['id_kecamatan'];
        return $this->db->query('select * from tb_desa where kecamatan_id ='.$id_kecamatan.'')->result();
	}


}