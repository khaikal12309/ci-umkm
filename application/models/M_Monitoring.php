<?php defined('BASEPATH') OR exit ('No direct script access allowed');

class M_Monitoring extends CI_Model
{
	private $_table = "tb_laporan";

	public function getAll()
	{
		return $this->db->query('select m.*, u.name as nama_umkm, u.id as id_umkm, p.name as nama_user, p.id as id_user from tb_laporan m LEFT JOIN tb_umkm u on m.umkm_id = u.id left join tb_user p ON m.user_id = p.id')->result();
	}

	public function getByUmkm($id_umkm)
	{
		return $this->db->query('select * from tb_laporan where umkm_id = '.$id_umkm.'')->result();
	}

	public function getByDate()
	{
		$month = date('m') - 1;
		return $this->db->query('select m.*, u.name as nama_umkm, u.id as id_umkm, p.name as nama_user, p.id as id_user from tb_laporan m LEFT JOIN tb_umkm u on m.umkm_id = u.id left join tb_user p ON m.user_id = p.id where month(m.create_date) < '.$month.'')->result();
	}

	public function save()
	{
		$user_id = $this->session->userdata('id');
		$this->pemasukan = $_POST["pemasukan"];
		$this->pengeluaran = $_POST["pengeluaran"];
		$this->user_id = $user_id;
		$this->umkm_id = $_POST["umkm_id"];

		return $umkm = $this->db->insert($this->_table, $this);
	}

}