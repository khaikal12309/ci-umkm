<?php defined('BASEPATH') OR exit ('No direct script access allowed');

class M_User extends CI_Model
{
	private $_table = "tb_user";

	
	public function check_login($data)
	{
		return $this->db->get_where($this->_table, ["email" => $data['email'],"password" => $data['password']]);
	}

	public function getAll()
	{
		$this->db->select('*');
		$this->db->from('tb_user');
		return $this->db->get()->result();
	}

	public function getMe(){
		$user_id = $this->session->userdata('id');
		$this->db->select('*');
		$this->db->from('tb_user');
		$this->db->where('id ='.$user_id);
		return $this->db->get()->result();
	}
	public function getUser()
	{
		$this->db->select('*');
		$this->db->from('tb_user');
		$this->db->where('state != "admin"');
		return $this->db->get()->result();
	}

	public function save()
	{
		$this->name = $_POST["name"];
		$this->email = $_POST["email"];
		$this->password = md5($_POST["password"]);
		$this->state = $_POST["state"];
		$this->address = $_POST["address"];
		$this->phone = $_POST["phone"];
		$this->is_active = 1;

		return $news = $this->db->insert($this->_table, $this);

	}

	public function update(){
		$this->name = $_POST["name"];
		$this->email = $_POST["email"];
		$this->state = $_POST["state"];
		$this->address = $_POST["address"];
		$this->phone = $_POST["phone"];
		$this->is_active = 1;
		
		return $this->db->update($this->_table, $this, array('id' => $_POST['id']));
	}

	public function delete($id)
	{
		return $this->db->delete($this->_table, array("id" => $id));
	}
}