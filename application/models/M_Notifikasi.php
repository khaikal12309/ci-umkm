<?php defined('BASEPATH') OR exit ('No direct script access allowed');

class M_Notifikasi extends CI_Model
{
	private $_table = "tb_notifikasi";

	public function getAll()
	{
        return $this->db->query('select n.*, user.name as nama_user, user.id as id_user, umkm.name as nama_umkm 
         from tb_notifikasi n LEFT JOIN tb_umkm umkm on n.umkm_id = umkm.id left join tb_user user ON umkm.user_id = user.id')->result();
	}

	public function getUnread()
	{
        return $this->db->query('select n.*, user.name as nama_user, user.id as id_user, umkm.name as nama_umkm 
         from tb_notifikasi n LEFT JOIN tb_umkm umkm on n.umkm_id = umkm.id left join tb_user user ON umkm.user_id = user.id where n.is_read = 0')->result();
	}

	public function save($umkm_id=NULL)
	{
		$this->umkm_id = $umkm_id;

		return $umkm = $this->db->insert($this->_table, $this);
	}

}