<?php defined('BASEPATH') OR exit ('No direct script access allowed');

class M_Kategori extends CI_Model
{
	private $_table = "tb_kategori_umkm";

	public function getAll()
	{
        return $this->db->query('select * from tb_kategori_umkm')->result();
	}

	public function save($umkm_id=NULL)
	{
		$this->umkm_id = $umkm_id;

		return $umkm = $this->db->insert($this->_table, $this);
	}

}