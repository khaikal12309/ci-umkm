<?php defined('BASEPATH') OR exit ('No direct script access allowed');

class M_Umkm extends CI_Model
{
	private $_table = "tb_umkm";

	public function getAll()
	{
		$key = '';
		if ($_POST){
			if ($_POST['kecamatan_id']){
				$key .= 'where u.kecamatan_id ='.$_POST['kecamatan_id'];
				if ($_POST['desa_id']){
					$key .= ' and u.desa_id ='.$_POST['desa_id'];
				}
				if ($_POST['qword']){
					$key .= ' and u.name like "%'.$_POST['qword'].'%"';
				}
			} else{
				if ($_POST['qword']){
					$key .= 'where u.name like "%'.$_POST['qword'].'%"';
				}
			}
		}
		return $this->db->query('select u.id as id_umkm,u.name as nama_umkm, u.is_active, p.id as id_user, p.name as nama_user from tb_umkm u left join tb_user p on u.user_id = p.id '.$key)->result();
		// $this->db->from('tb_umkm');
		// return $this->db->get()->result();
	}

	public function getMe()
	{
		$key = '';
		if ($_POST){
			if ($_POST['kecamatan_id']){
				$key .= ' and u.kecamatan_id ='.$_POST['kecamatan_id'];
			}
			if ($_POST['desa_id']){
				$key .= ' and u.desa_id ='.$_POST['desa_id'];
			}
			if ($_POST['qword']){
				$key .= ' and u.name like"%'.$_POST['qword'].'%"';
			}
		}
		$user_id = $this->session->userdata('id');
		print_r($key);
		die();
		return $this->db->query('select u.id as id_umkm,u.name as nama_umkm, u.is_active, p.id as id_user, p.name as nama_user from tb_umkm u left join tb_user p on u.user_id = p.id where u.user_id='.$user_id.=$key)->result();
		// $this->db->from('tb_umkm');
		// return $this->db->get()->result();
	}

	public function getById($id_umkm)
	{
		$user_id = $this->session->userdata('id');
		return $this->db->query('select u.id as id_umkm,u.name as nama_umkm, u.is_active, p.id as id_user, p.name as nama_user, COUNT(t.id) as total_product from tb_umkm u left join tb_user p on u.user_id = p.id left join tb_product t on t.umkm_id = u.id where u.id='.$id_umkm.' GROUP by (t.umkm_id) ')->result();
		// $this->db->from('tb_umkm');
		// return $this->db->get()->result();
	}

	public function getActive()
	{
		return $this->db->query('select u.id as id_umkm,u.name as nama_umkm, u.is_active, p.id as id_user, p.name as nama_user, COUNT(t.id) as total_product from tb_umkm u left join tb_user p on u.user_id = p.id left join tb_product t on t.umkm_id = u.id where u.is_active = 1 GROUP by (t.umkm_id)')->result();
		// $this->db->from('tb_umkm');
		// return $this->db->get()->result();
	}

	public function save()
	{
		$user_id = $this->session->userdata('id');
		$this->name = $_POST["name"];
		$this->user_id = $user_id;
		$this->kecamatan_id = $_POST["kecamatan_id"];
		$this->desa_id = $_POST["desa_id"];
		$this->is_active = 1;

		return $this->db->insert($this->_table, $this);

	}

	public function update(){
		$this->name = $_POST["name"];
		$this->user_id = $_POST["user_id"];
		$this->is_active = $_POST["is_active"];
		return $this->db->update($this->_table, $this, array('id' => $_POST['id']));
	}

	public function NonActive($id=Null){
		$this->is_active = 0;

		return $this->db->update($this->_table, $this, array('id' => $id));
	}

	public function delete($id)
	{
		return $this->db->delete($this->_table, array("id" => $id));
	}
}