<?php defined('BASEPATH') OR exit ('No direct script access allowed');

class M_Product extends CI_Model
{
	private $_table = "tb_product";

	public function getAll()
	{
		return $this->db->query('select u.id as id_umkm, u.name as nama_umkm, p.* from tb_product p LEFT JOIN tb_umkm u on p.umkm_id = u.id order by create_date desc')->result();
		// $this->db->from('tb_umkm');
		// return $this->db->get()->result();
    }
    
    public function getByUmkm($id_umkm)
	{
		return $this->db->query('select u.id as id_umkm, u.name as nama_umkm, p.* from tb_product p LEFT JOIN tb_umkm u on p.umkm_id = u.id where u.id = '.$id_umkm.'')->result();
		// $this->db->from('tb_umkm');
		// return $this->db->get()->result();
	}

	public function save()
	{
		$this->name = $_POST["name"];
		$this->price = $_POST["price"];
		$this->qty = $_POST["qty"];
		$this->umkm_id = $_POST["umkm_id"];

		$this->image = $this->_uploadImage();
		return $news = $this->db->insert($this->_table, $this);

	}

	public function update(){
		$this->name = $_POST["name"];
		$this->price = $_POST["price"];
		$this->qty = $_POST["qty"];
		$this->umkm_id = $_POST["umkm_id"];
		if (!empty($_FILES["image"]["name"])) {
            $this->image = $this->_uploadImage();
        } else {
            $this->image = $_POST["old_image"];
        }

		return $this->db->update($this->_table, $this, array('id' => $_POST['id']));
	}

	public function delete($id)
	{
		$this->_deleteImage($id);
		return $this->db->delete($this->_table, array("id" => $id));
	}
	private function _uploadImage()
    {
        $config['upload_path']          = './images/product';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = 'img_'.$_POST["name"];
        $config['overwrite']            = true;
        $config['max_size']             = 4000; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('image')) {
            return $this->upload->data("file_name");
        }

        $errors = $this->upload->display_errors();
        return  "default.png";
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }
    private function _deleteImage($id)
    {
        $product = $this->getById($id);
        if ($product->image != "default.jpg") {
            $filename = explode(".", $product->image)[0];
            return array_map('unlink', glob(FCPATH."image/product/$filename.*"));
        }
    }
}