<div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url()?>assets/img/logo.png">
	<div class="logo"><a href="http://www.creative-tim.com" class="simple-text logo-normal">
		SIMON UMKM
	</a></div>
	<div class="sidebar-wrapper">
		<ul class="nav">
			<?php if ($this->session->userdata('state') == 'admin'){?>
				<li class="nav-item <?php if ($page == "dashboard"){ echo 'active';}?>">
					<a class="nav-link" href="<?php echo base_url();?>index.php/admin/adminDashboard">
						<i class="material-icons">dashboard</i>
						<p>Dashboard</p>
					</a>
					 <!--  -->
				</li>
				<li class="nav-item <?php if ($page == "user"){ echo 'active';}?>">
					<a class="nav-link" href="<?php echo base_url();?>index.php/admin/User">
						<i class="material-icons">person</i>
						<p>Data User</p>
					</a>
				</li>
				<li class="nav-item <?php if ($page == "umkm"){ echo 'active';}?>">
					<a class="nav-link" href="<?php echo base_url();?>index.php/admin/Umkm">
						<i class="material-icons">store</i>
						<p>Data Umkm</p>
					</a>
				</li>
				<li class="nav-item <?php if ($page == "product"){ echo 'active';}?>">
					<a class="nav-link" href="<?php echo base_url();?>index.php/admin/Product">
						<i class="material-icons">library_books</i>
						<p>Data Product</p>
					</a>
				</li>
			<?php }?>
			<li class="nav-item <?php if ($page == "monitoring"){ echo 'active';}?>">
				<a class="nav-link" href="<?php echo base_url();?>index.php/admin/Monitoring">
					<i class="material-icons">auto_stories</i>
					<p>Monitoring UMKM</p>
				</a>
			</li>
			<?php if ($this->session->userdata('state') == 'admin'){?>
				<li class="nav-item active-pro <?php if ($page == "config"){ echo 'active';}?>" >
					<a class="nav-link" href="<?php echo base_url();?>index.php/admin/Configuration" style="vertical-align: bottom;">
						<i class="material-icons">settings</i>
						<p>Configuration</p>
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</div>