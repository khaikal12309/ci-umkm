<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
	<div class="container-fluid">
		
		<button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
			<span class="sr-only">Toggle navigation</span>
			<span class="navbar-toggler-icon icon-bar"></span>
			<span class="navbar-toggler-icon icon-bar"></span>
			<span class="navbar-toggler-icon icon-bar"></span>
		</button>
		<div class="collapse navbar-collapse justify-content-end">
			<form class="navbar-form">
				<div class="input-group no-border">
					<input type="text" value="" class="form-control" id="search" placeholder="Search...">
					<!-- <button type="submit" class="btn btn-white btn-round btn-just-icon"> -->
						<!-- <i class="material-icons">search</i>
						<div class="ripple-container"></div>
					</button> -->
				</div>
				<script src="https://code.jquery.com/jquery-3.5.1.js"></script>	
				<script type="text/javascript">
					$("#search").keyup(function () {
						var value = this.value.toLowerCase().trim();

						$("table tr").each(function (index) {
							if (!index) return;
							$(this).find("td").each(function () {
								var id = $(this).text().toLowerCase().trim();
								var not_found = (id.indexOf(value) == -1);
								$(this).closest('tr').toggle(!not_found);
								return not_found;
							});
						});
					});
				</script>
			</form>
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="javascript:;">
						<i class="material-icons">dashboard</i>
						<p class="d-lg-none d-md-block">
							Stats
						</p>
					</a>
				</li>
				<li class="nav-item dropdown">
					<?php 
						$this->load->model('M_Notifikasi');
						$notifications = $this->M_Notifikasi->getAll();
						$unreads = $this->M_Notifikasi->getUnread();
					?>
					<a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="material-icons">notifications</i>
						<span class="notification"><?php echo count($unreads);?></span>
						<p class="d-lg-none d-md-block">
							Some Actions
						</p>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
						<?php foreach ($notifications as $notification):?>
						<a class="dropdown-item" href="#"><?php echo $notification->nama_user;?> add new umkm <?php echo $notification->nama_umkm;?></a>
						<?php endforeach; ?>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="material-icons">person</i>
						<p class="d-lg-none d-md-block">
							Account
						</p>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
						<!-- <a class="dropdown-item" href="#">Profile</a>
						<a class="dropdown-item" href="#">Settings</a> -->
						<!-- <div class="dropdown-divider"></div> -->
						<a class="dropdown-item" href="<?php echo base_url()?>index.php/Auth/logout">Log out</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>