<section class="section bg-grey" id="feature" style="padding-top:3%">
	<div class="container">
		<div class="row justy-content-center" id='content'>
			<?php if ($page == 'myumkm') { ?>
				<div class="col-12" style="margin-top:3%">
					<center>
						<h1 style="background-color: #21c87a !important;color:white;padding:15px">UMKM SAYA</h1>
					</center>
				</div>
			<?php } ?>
			<?php if ($page == 'myumkm') { ?>
			<div class="col-12" style="margin-top:3%">
				<form method="POST" action="<?php echo base_url();?>index.php/Home/myumkm">
					<div class="row p-0 m-0">
						<select class="form-control col-2 mr-2" name="kecamatan_id" id="kecamatan_search">
							<option selected="" value="">Kecamatan</option>
							<?php foreach ($kecamatans as $kecamatan):?>
								<option value="<?php echo $kecamatan->id;?>"><?php echo $kecamatan->name;?></option>
							<?php endforeach;?>
						</select>
						
						<select class="form-control col-2 mr-2" name="desa_id" id="desa_search">
							<option selected="" value="">Desa</option>
						</select>

						<input type="text" name="qword" id="searchUMKM" placeholder="Cari umkm...." class="form-control col-lg-5 mr-2">
						
						<button type="submit" class="btn btn-info col-2 p-0">Cari..</button>
					
					</div>
				</form>
			</div>
			<?php }else{?>
			<div class="col-12" style="margin-top:3%">
				<form method="POST" action="<?php echo base_url();?>index.php/Home/umkm">
					<div class="row p-0 m-0">
						<select class="form-control col-2 mr-2" name="kecamatan_id" id="kecamatan_search">
							<option selected="" value="">Kecamatan</option>
							<?php foreach ($kecamatans as $kecamatan):?>
								<option value="<?php echo $kecamatan->id;?>"><?php echo $kecamatan->name;?></option>
							<?php endforeach;?>
						</select>
						
						<select class="form-control col-2 mr-2" name="desa_id" id="desa_search">
							<option selected="" value="">Desa</option>
						</select>

						<input type="text" name="qword" id="searchUMKM" placeholder="Cari umkm...." class="form-control col-lg-5 mr-2">
						
						<button type="submit" class="btn btn-info col-2 p-0">Cari..</button>
					
					</div>
				</form>
			</div>
			<?php } ?>
			<?php if ($page == 'myumkm') { ?>
			<div class="col-12" style="margin-top:3%">
				<button class="btn btn-primary btn-xm pt-4" data-toggle="modal" data-target="#addUMKM" margin-top:3%>Tambah Data</button>
			</div>
			<?php }?>
			<?php foreach($umkms as $umkm):?>
					<div class="col-lg-4 col-sm-6 col-md-6" style="margin-top:3%">
						<div class="text-center feature-block">
							<div class="card">
								<div class="card-header">
									<?php echo $umkm->nama_umkm; ?>
									<div style="float:right">
										<?php if ($umkm->is_active){?>
											<span class="badge badge-success">Aktif</span>
										<?php } else {?>
											<span class="badge badge-dange">Nonaktif</span>
										<?php }?> 
									</div>
								</div>
								<div class="card-body">
									<div class="card-text"> 
										Pemilik: <?php echo $umkm->nama_user;?> <br/>
									</div>
								</div>
								<?php if ($page == 'myumkm') { ?>
									<div class="card-footer">
										<a href="<?php echo base_url('index.php/user/Home/manage_umkm/'.$umkm->id_umkm.'')?>" class="btn btn-success btn-xs">Manage</a>
										<a href="<?php echo base_url('index.php/user/Home/delete_umkm/'.$umkm->id_umkm.'')?>" class="btn btn-danger btn-xs">Delete</a>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
			<?php endforeach;?>
		</div>
	</div> <!-- / .container -->
</section>
<script src="<?php echo base_url();?>assets/rappo/plugins/jquery/jquery.min.js"></script>

<div class="modal fade" id="addUMKM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>index.php/user/Home/addUmkm">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Tambah Data UMKM</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="Nama UMKM" name="name"/>
					</div>
					<div class="input-group mb-3">
						<select class="form-control" name="kategori_id">
							<option selected="" value="#">Pilih Kategori UMKM</option>
							<?php foreach($kategoris as $kategori):?>
								<option value="$kategpri"><?php echo $kategori->name;?></option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="input-group mb-3">
						<select class="form-control" name="kecamatan_id" id="kecamatan">
							<option selected="" value="#">Kecamatan</option>
							<?php foreach ($kecamatans as $kecamatan):?>
								<option value="<?php echo $kecamatan->id;?>"><?php echo $kecamatan->name;?></option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="input-group mb-3">
						<select class="form-control" name="desa_id" id="desa">
							<option selected="" value="#">Desa</option>
						</select>
					</div>
					<input type="hidden" class="form-control" name="umkm_id" value="<?php echo $umkm->id_umkm;?>"/>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	$( document ).ready(function() {
		$("#kecamatan").change(function(){
		var id_kecamatan = $(this).val(); 
			$.ajax({
				url: "<?php echo base_url();?>index.php/Home/getDesa",
				method : "POST",
				data : {id_kecamatan: id_kecamatan},
				async : false,
				dataType : 'json',
				success: function(data){
					var html = '';
					
					var i;
					for(i=0; i<data.length; i++){
						html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
					}
					if (html === ''){
						var html = '<option value="" selected>Tidak ada desa</option>'
					}
					$('#desa').html(html);
						
				}
			});                    
		});

		$("#kecamatan_search").change(function(){
		var id_kecamatan = $(this).val(); 
			$.ajax({
				url: "<?php echo base_url();?>index.php/Home/getDesa",
				method : "POST",
				data : {id_kecamatan: id_kecamatan},
				async : false,
				dataType : 'json',
				success: function(data){
					var html = '';
					var i;
					for(i=0; i<data.length; i++){
						html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
					}
					if (html === ''){
						var html = '<option value="" selected>Tidak ada desa</option>'
					}
					$('#desa_search').html(html);
						
				}
			});                    
		});  
	
	});
</script>