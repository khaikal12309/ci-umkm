<section class="section">
		<!-- Content -->
	<div class="container">
		<div class="row  align-items-center">
			<div class="col-lg-4"></div>
			<div class="col-md-12 col-lg-4 text-center text-lg-left">
				<div class="main-banner">
					<div class="card">
						<form method="POST" action="<?php echo base_url()?>index.php/Auth/do_login">
						<div class="card-header bg-primary"><h4>Login</h4></div>
						<div class="card-body">
							<div class="form-group">
								<input type="email" class="form-control" name="email" placeholder="Email" required="" />
							</div>
							<div class="form-group">
								<input type="password"  class="form-control" name="password" placeholder="Password" required="" />
							</div>
							<div class="form-group">
								<button type="submit"  class="btn btn-primary">Login</button>
							</div>
							<div class="form-group">
								Belum punya akun? <a href="<?php echo base_url()?>index.php/Auth/register">Daftar</a>
							</div>
						</div>
					</div>
					
				</div>
			</div>

			
		</div> <!-- / .row -->
	</div> <!-- / .container -->
</section>
<script src="<?php echo base_url();?>assets/rappo/plugins/jquery/jquery.min.js"></script>