<section class="section bg-grey" id="feature" style="padding-top:3%">
	<div class="container">
		<div class="row justy-content-center">
			<div class="col-12" style="margin-top:3%">
				<div class="row">
					<div class="card text-center col-6">
						<div class="card-header">
							<h1>Profile</h1>
						</div>
						<div class="card-body">
							<h5 class="card-title"><?php echo $user->name?></h5>
							<p class="card-text">
								<?php echo $user->email?> - <?php echo $user->phone;?> <br/>
								<?php echo $user->address?>
								
							</p>
							
						</div>
						<div class="card-footer text-muted">
							<button class="btn btn-warning" style="color:white;padding:15px;float: left;"><span class="fa fa-key"/> Change Password</button>
							<a href="#" class="btn btn-primary" style="color:white;padding:15px; float: right;">Update</a>
						</div>
					</div>
					<div class="col-2"></div>
					<div class="card card-info col-4">
					  <div class="card-header">
					    <h5 class="card-title">UMKM SAYA</h5>
					  </div>
					  <ul class="list-group list-group-flush">
					    <li class="list-group-item">Cras justo odio</li>
					    <li class="list-group-item">Dapibus ac facilisis in</li>
					    <li class="list-group-item">Vestibulum at eros</li>
					  </ul>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- / .container -->
</section>
<script src="<?php echo base_url();?>assets/rappo/plugins/jquery/jquery.min.js"></script>