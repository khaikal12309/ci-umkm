<div class="logo-bar d-none d-md-block d-lg-block bg-light">
	<div class="container">
		<div class="row">
			<div class="col-lg-2">
				<div class="logo d-none d-lg-block">
					<!-- Brand -->
					<a class="navbar-brand js-scroll-trigger" href="index.html">
						<h2>UMKM SUBANG</h2>
					</a>
				</div>
			</div>

			<div class="col-lg-8 justify-content-end ml-lg-auto d-flex col-12 col-md-12 justify-content-md-center">

				<div class="top-info-block d-inline-flex">
					<div class="icon-block">
						<i class="ti-phone"></i>
					</div>
					<div class="info-block">
						<h5 class="font-weight-500">(0260) 412112</h5>
						<p>Fax/ Phone</p>
					</div>
				</div>
				<div class="top-info-block d-inline-flex">
					<div class="icon-block">
						<i class="ti-time"></i>
					</div>
					<div class="info-block">
						<h5 class="font-weight-500">Mon-Sat 9:00-12.00 </h5>
						<p>Sunday Closed</p>
					</div>
				</div>
				<div class="top-info-block d-inline-flex">
					
					<div class="icon-block">
						<i class="fa fa-user"></i>
					</div>
					<div class="info-block">
						<a href="<?php echo base_url();?>index.php/user/Home/profile">
							<h5 class="font-weight-500"><?php echo $this->session->userdata('name')?></h5>
							<p><?php echo $this->session->userdata('email')?></p>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="main-navigation" id="mainmenu-area">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary main-nav navbar-togglable rounded-radius">

			<a class="navbar-brand d-lg-none d-block" href="">
				<h4>JARGAS</h4>
			</a>
			<!-- Toggler -->
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fa fa-bars"></span>
			</button>

			<!-- Collapse -->
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<!-- Links -->
				<ul class="navbar-nav ">
					<li class="nav-item" <?php if ($page == 'home'){ echo"style='background-color: #049551'";}?>>
						<a class="nav-link active" href="index.html">
							Home
						</a>
					</li>
					<li class="nav-item" <?php if ($page == 'umkm'){ echo"style='background-color: #049551'";}?>>
						<a href="<?php echo base_url()?>index.php/Home/umkm" class="nav-link js-scroll-trigger">
							UMKM
						</a>
					</li>
					<?php if ($this->session->userdata('id')){?>
						<li class="nav-item" <?php if ($page == 'myumkm'){ echo"style='background-color: #049551'";}?>>
							<a href="<?php echo base_url()?>index.php/Home/myumkm" class="nav-link js-scroll-trigger">
								My UMKM
							</a>
						</li>
					<?php } ?>
					<li class="nav-item " <?php if ($page == 'products'){ echo"style='background-color: #049551'";}?>>
						<a href="<?php echo base_url()?>index.php/Home/products" class="nav-link js-scroll-trigger">
							Products
						</a>
					</li>
					<li class="nav-item ">
						<a href="<?php echo base_url()?>index.php/Home/about" class="nav-link js-scroll-trigger">
							About
						</a>
					</li>
					<li class="nav-item ">
						<a href="<?php echo base_url()?>index.php/Home/umkm" class="nav-link">
							Contact
						</a>
					</li>
				</ul>

				<ul class="ml-lg-auto list-unstyled m-0">
					<?php if ($this->session->userdata('id')){?>
						<li><a href="<?php echo base_url();?>index.php/Auth/logout" class="btn btn-white btn-circled">Logout</a></li>
					<?php }else{?>
					<li><a href="<?php echo base_url();?>index.php/Auth" class="btn btn-white btn-circled">Login</a></li>
					<?php } ?>
				</ul>
			</div> <!-- / .navbar-collapse -->
		</nav>
	</div> <!-- / .container -->
</div>