<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="it">
  <meta name="keywords" content="Rapoo,creative, agency, startup,onepage, clean, modern,business, company,it">
  
  <meta name="author" content="themefisher.com">

  <title>UMKM</title>

  <!-- bootstrap.min css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/rappo/plugins/bootstrap/css/bootstrap.min.css">
  <!-- Animate Css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/rappo/plugins/animate-css/animate.css">
  <!-- Icon Font css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/rappo/plugins/fontawesome/css/all.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/rappo/plugins/fonts/Pe-icon-7-stroke.css">
  <!-- Themify icon Css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/rappo/plugins/themify/css/themify-icons.css">
  <!-- Slick Carousel CSS -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/rappo/plugins/slick-carousel/slick/slick.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/rappo/plugins/slick-carousel/slick/slick-theme.css">

  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/rappo/css/style.css">

</head>


<body id="top-header">

<!-- LOADER TEMPLATE -->
	<div id="page-loader">
		<div class="loader-icon fa fa-spin colored-border"></div>
	</div>
	<!-- Header -->
	<?php echo $header;?>



	<!-- Content -->
	<?php echo $content;?>



	<!-- FOOTER -->
	<?php echo $footer;?>
 	
</body>


	<!--  Page Scroll to Top  -->

	<a class="scroll-to-top js-scroll-trigger" href="#top-header">
		<i class="fa fa-angle-up"></i>
	</a>


   


	<!-- 
	Essential Scripts
	=====================================-->

	
	<!-- Main jQuery -->
	
	
	<!-- Bootstrap 3.1 -->
	
  <!-- <script src="<?php echo base_url()?>assets/assets/js/core/jquery.min.js"></script> -->
	<script src="<?php echo base_url();?>assets/rappo/plugins/bootstrap/js/popper.min.js"></script>
	<script src="<?php echo base_url();?>assets/rappo/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- Slick Slider -->
	<script src="<?php echo base_url();?>assets/rappo/plugins/slick-carousel/slick/slick.min.js"></script>
	<!-- <script src="<?php echo base_url();?>assets/rappo/js/jquery.easing.1.3.js"></script> -->
	<!-- Map Js -->
	<!-- <script src="<?php echo base_url();?>assets/rappo/plugins/google-map/gmap3.min.js"></script> -->
	<!-- <script src="<?php echo base_url();?>assets/rappo/https://maps.googleapis.com/maps/api/js?key=AIzaSyDwIQh7LGryQdDDi-A603lR8NqiF3R_ycA"></script> -->

	<!-- <script src="<?php echo base_url();?>assets/rappo/js/form/contact.js"></script> -->
	<script src="<?php echo base_url();?>assets/rappo/js/theme.js"></script>

  </body>
  </html>
   