 <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"> -->
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css"> 
<!-- https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css -->
<div class="content" style="padding-top: 0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-6 col-sm-6">
				<div class="card">
					<div class="card-header card-header-success text-center">
						<h3 class="card-title">Data Laporan</h3>
					</div>
					<div class="card-body">
						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#addData">
							<i class="material-icons">print</i> Print Data
						</a>
						<a href="#" class="btn btn-success" data-toggle="modal" data-target="#addData">
							<i class="material-icons">print</i> Print Data
						</a>
						<table class="table table-stipped" id="TableLaporan">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th>Pemasukan</th>
									<th>Pengeluaran</th>
									<th>UMKM</th>
									<th>Pelapor</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$no = 1;
								foreach($monitorings as $monitoring):?>
								<tr>
									<td class="text-center"><?php echo $no; $no++?></td>
									<td><?php echo $monitoring->pemasukan;?></td>
									<td><?php echo $monitoring->pengeluaran;?></td>
									<td><?php echo $monitoring->nama_umkm;?></td>
									<td><?php echo $monitoring->nama_user;?></td>
									<td><?php echo $monitoring->create_date;?></td>
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php if($this->session->flashdata('error')):?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert" style="position: absolute; right: 3%; bottom: 2%">
				<?php echo $this->session->flashdata('error');?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif ?>	
			<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-success alert-dismissible fade show" role="alert" style="position: absolute; right: 3%; bottom: 2%">
				<?php echo $this->session->flashdata('success');?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif ?>	 
		</div>
	</div>
</div>

<!-- Plugin for the momentJs  -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script> -->

<script type="text/javascript">
$(document).ready(function() {
    $('#TableLaporan').DataTable({
    	// "paging":   false,
        "ordering": false,
        "searching": false,
    });
} );
</script>