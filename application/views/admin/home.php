<!-- End Navbar -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header card-header-warning card-header-icon">
						<div class="card-icon">
							<i class="material-icons">person</i>
						</div>
						<p class="card-category">User</p>
						<h3 class="card-title">3
							<small>GB</small>
						</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header card-header-success card-header-icon">
						<div class="card-icon">
							<i class="material-icons">store</i>
						</div>
						<p class="card-category">UMKM</p>
						<h3 class="card-title">34</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header card-header-danger card-header-icon">
						<div class="card-icon">
							<i class="material-icons">analytics</i>
						</div>
						<p class="card-category">Product</p>
						<h3 class="card-title">75</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header card-header-info card-header-icon">
						<div class="card-icon">
							<i class="fa fa-book"></i>
						</div>
						<p class="card-category">Laporan</p>
						<h3 class="card-title">+245</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
