 <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"> -->
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css"> 
<!-- https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css -->
<div class="content" style="padding-top: 0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="card">
					<form method="post" action="<?php echo base_url()?>index.php/admin/Configuration/do_add_periode_laporan">
						<div class="card-header card-header-success text-center">
							<h3 class="card-title">Periode Laporan</h3>
						</div>
						<div class="card-body">
							<div class="form-group bmd-form-group">
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="material-icons">calendar_today</i></div>
									</div>
									<?php foreach($configs as $config):?>
										<?php if($config->code == "PER-LAP"){?>
											<input type="text" name="period" class="form-control" placeholder="Due Date Laporan" value="<?php echo $config->period;?>">
										<?php }?>
									<?php endforeach;?>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button type="submit" class="btn btn-success">Save</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="card">
					<div class="card-header card-header-warning text-center">
						<h3 class="card-title">Cron Job</h3>
					</div>
					<div class="card-body">
						<div class="form-group bmd-form-group">
							<table class="table">
								<tr>
									<th>Schedule</th>
									<th>Periode</th>
									<th>Action</th>
								</tr>
								<tr>
									<td>Cek Laporan</td>
									<td>1 Day</td>
									<td>
										<button id="run_cron" class="btn btn-success btn-sm">
							                <i class="fa fa-reddit"></i>
							             </button>
          							</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?php if($this->session->flashdata('error')):?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert" style="position: absolute; right: 3%; bottom: 2%">
				<?php echo $this->session->flashdata('error');?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif ?>	
			<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-success alert-dismissible fade show" role="alert" style="position: absolute; right: 3%; bottom: 2%">
				<?php echo $this->session->flashdata('success');?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif ?>	 
		</div>
	</div>
</div>

<!-- Plugin for the momentJs  -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script> -->

<script type="text/javascript">
$(document).ready(function() {
    $('#TableLaporan').DataTable({
    	// "paging":   false,
        "ordering": false,
        "searching": false,
    });
} );
</script>

<!-- <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> -->
<script type="text/javascript">
$(document).ready(function() {   
   $('#run_cron').click(function(){
    var url = "<?php echo base_url('index.php/admin/Configuration/run_cron') ?>";

    $.post(url, function(data) {   

        alert(data.msg)
        // console.log(json)
    }, "json");
  });
});
</script>