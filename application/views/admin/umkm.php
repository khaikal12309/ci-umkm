 <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"> -->
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css"> 
<!-- https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css -->
<div class="content" style="padding-top: 0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-6 col-sm-6">
				<div class="card">
					<div class="card-header card-header-success text-center">
						<h3 class="card-title">Data Umkm</h3>
					</div>
					<div class="card-body"> 
						<?php if ($this->session->userdata('state') == 'admin'){?>
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addData">
							Tambah Data
							</button>
						<?php }?>
						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#addData">
							<i class="material-icons">print</i> Print Data
						</a>
						<a href="#" class="btn btn-success" data-toggle="modal" data-target="#addData">
							<i class="material-icons">print</i> Print Data
						</a>
						<table class="table table-stipped" id="Tableumkm">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th>Nama UMKM</th>
									<th>Pemilik</th>
									<th class="text-right">Status</th>
									<?php if ($this->session->userdata('state') == 'admin'){?>
										<th class="text-right">Actions</th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<?php 
								$no = 1;
								foreach($umkms as $umkm):?>
								<tr>
									<td class="text-center"><?php echo $no; $no++?></td>
									<td><?php echo $umkm->nama_umkm;?></td>
									<td><?php echo $umkm->nama_user;?></td>
									<td class="text-right">
										<?php if ($umkm->is_active == 1)
										{
											echo "<span class='badge badge-primary'>Active</span>";
										}
										else
										{ 
											echo "<span class='badge badge-danger'>Non Active</span>";
										}
										?>
										
									</td>
									<?php if ($this->session->userdata('state') == 'admin'){?>
										<td class="td-actions text-right">
											<button type="button" rel="tooltip" class="btn btn-success btn-round" data-toggle="modal" data-target="#edit<?php echo $umkm->id_umkm;?>">
												<i class="material-icons">edit</i>
											</button>
											<button type="button" rel="tooltip" class="btn btn-danger btn-round" data-toggle="modal" data-target="#delete<?php echo $umkm->id_umkm;?>">
												<i class="material-icons">close</i>
											</button>
										</td>
									<?php }?>
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php if($this->session->flashdata('error')):?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert" style="position: absolute; right: 3%; bottom: 2%">
				<?php echo $this->session->flashdata('error');?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif ?>	
			<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-success alert-dismissible fade show" role="alert" style="position: absolute; right: 3%; bottom: 2%">
				<?php echo $this->session->flashdata('success');?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif ?>	 
		</div>
	</div>
</div>



<div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Data umkm</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form class="form" method="post" action="<?php echo base_url()?>index.php/admin/umkm/do_add">
			<div class="modal-body" style="padding-left: 0; padding-top: 0; padding-bottom: 0">
				<div class="form-group bmd-form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="material-icons">store</i></div>
						</div>
						<input type="text" name="name" class="form-control" placeholder="name">
					</div>
				</div>
			</div>
	  		<div class="modal-footer">
			<select class="form-control" name="user_id">
			<?php 
			foreach($users as $user):?>
				<option value="#" selected="">User</option>
			  	<option value="<?php echo $user->id;?>"><?php echo $user->name;?></option>
			<?php endforeach; ?>
		  	</select>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
	  		</div>
			</form>
		</div>
  	</div>
</div>

<?php foreach($umkms as $umkm):?>
<div class="modal fade" id="delete<?php echo $umkm->id_umkm;?>" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-body">
		Are you sure want to delete <?php echo $umkm->nama_umkm;?>?
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<a href="<?php echo base_url('index.php/admin/umkm/delete/'.$umkm->id_umkm.'')?>" class="btn btn-primary">Confirm</a>
	  </div>
	</div>
  </div>
</div>
<?php endforeach;?>


<?php foreach($umkms as $umkm):?>
<div class="modal fade" id="edit<?php echo $umkm->id_umkm;?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit Data umkm</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form class="form" method="post" action="<?php echo base_url()?>index.php/admin/umkm/do_edit">
			<div class="modal-body" style="padding-left: 0; padding-top: 0; padding-bottom: 0">
				<input type="hidden" name="id" class="form-control" value="<?php echo $umkm->id_umkm;?>">
				<div class="form-group bmd-form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="material-icons">store</i></div>
						</div>
						<input type="text" name="name" class="form-control" value="<?php echo $umkm->nama_umkm;?>">
					</div>
				</div>
			</div>
	  		<div class="modal-footer">
			<select class="form-control" name="user_id">
			<?php 
			foreach($users as $user):?>
			  	<option value="<?php echo $user->id;?>" <?php if($umkm->id_user == $user->id){ echo "selected";}?>><?php echo $user->name;?></option>
			<?php endforeach; ?>
		  	</select>

		  	<select class="form-control" name="is_active">
			
			  	<option value="1" <?php if($umkm->is_active == 1){ echo "selected";}?>>Active</option>
			  	<option value="0" <?php if($umkm->is_active == 0){ echo "selected";}?>>Non Active</option>
		  	</select>
				<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
				<button type="submit" class="btn btn-primary">Save changes</button>
	  		</div>
			</form>
		</div>
  	</div>
</div>
<?php endforeach;?>
<!-- Plugin for the momentJs  -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script> -->

<script type="text/javascript">
$(document).ready(function() {
    $('#Tableumkm').DataTable({
    	// "paging":   false,
        "ordering": false,
        "searching": false,
    });
} );
</script>