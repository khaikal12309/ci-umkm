 <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"> -->
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css"> 
<!-- https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css -->
<div class="content" style="padding-top: 0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-6 col-sm-6">
				<div class="card">
					<div class="card-header card-header-success text-center">
						<h3 class="card-title">Data Produk</h3>
					</div>
					<div class="card-body">
						<?php if ($this->session->userdata('state') == 'admin'){?>
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addData">
							Tambah Data
							</button>
						<?php }?>
						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#addData">
							<i class="material-icons">print</i> Print Data
						</a>
						<a href="#" class="btn btn-success" data-toggle="modal" data-target="#addData">
							<i class="material-icons">print</i> Print Data
						</a>
						<table class="table table-stipped" id="Tableumkm">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th>Nama Produk</th>
									<th>Harga</th>
									<th >Image</th>
									<th>Qty</th>
									<th>UMKM</th>
									<?php if ($this->session->userdata('state') == 'admin'){?>
										<th class="text-right">Actions</th>
									<?php }?>
								</tr>
							</thead>
							<tbody>
								<?php 
								$no = 1;
								foreach($products as $product):?>
								<tr>
									<td class="text-center"><?php echo $no; $no++?></td>
									<td><?php echo $product->name;?></td>
									<td><?php echo $product->price;?></td>
									<td>
										<div class="img-container">
											<img src="<?php if ($product->image){echo base_url('images/product/'.$product->image);}else{echo base_url('images/product/default.png');}?>" width="100px">
										</div>
									</td>
									<td><?php echo $product->qty;?></td>
									<td>
										<?php echo $product->nama_umkm;?>
									</td>
									<?php if ($this->session->userdata('state') == 'admin'){?>
										<td class="td-actions text-right">
											<button type="button" rel="tooltip" class="btn btn-success btn-round" data-toggle="modal" data-target="#edit<?php echo $product->id;?>">
												<i class="material-icons">edit</i>
											</button>
											<button type="button" rel="tooltip" class="btn btn-danger btn-round" data-toggle="modal" data-target="#delete<?php echo $product->id;?>">
												<i class="material-icons">close</i>
											</button>
										</td>
									<?php }?>
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php if($this->session->flashdata('error')):?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert" style="position: absolute; right: 3%; bottom: 2%">
				<?php echo $this->session->flashdata('error');?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif ?>	
			<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-success alert-dismissible fade show" role="alert" style="position: absolute; right: 3%; bottom: 2%">
				<?php echo $this->session->flashdata('success');?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif ?>	 
		</div>
	</div>
</div>



<div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Data Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" action="<?php echo base_url()?>index.php/admin/Product/do_add" enctype="multipart/form-data">
			<div class="modal-body" style="padding-left: 0; padding-top: 0; padding-bottom: 0">
				<div class="form-group bmd-form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="material-icons">all_inbox</i></div>
						</div>
						<input type="text" name="name" class="form-control" placeholder="Nama Produk">
					</div>
				</div>
				<div class="form-group bmd-form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="material-icons">money</i></div>
						</div>
						<input type="text" name="price" class="form-control" placeholder="Money">
					</div>
				</div>
				<div class="form-group bmd-form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="material-icons">production_quantity_limits</i></div>
						</div>
						<input type="text" name="qty" class="form-control" placeholder="Quantity">
					</div>
				</div>
				<div class="input-group">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">image</i></div>
					</div>
					<input type="file" name="image" class="form-control">
				</div>
			</div>
	  		<div class="modal-footer">
			<select class="form-control" name="umkm_id">
			<?php 
			foreach($umkms as $umkm):?>
				<option value="#" selected="">UMKM</option>
			  	<option value="<?php echo $umkm->id_umkm;?>"><?php echo $umkm->nama_umkm;?></option>
			<?php endforeach; ?>
		  	</select>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
	  		</div>
			</form>
		</div>
  	</div>
</div>

<?php foreach($products as $product):?>
<div class="modal fade" id="delete<?php echo $product->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-body">
		Are you sure want to delete <?php echo $product->name;?>?
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<a href="<?php echo base_url('index.php/admin/product/delete/'.$product->id.'')?>" class="btn btn-primary">Confirm</a>
	  </div>
	</div>
  </div>
</div>
<?php endforeach;?>


<?php foreach($products as $product):?>
<div class="modal fade" id="edit<?php echo $product->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit Data Product</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form class="form" method="post" action="<?php echo base_url()?>index.php/admin/Product/do_edit" enctype= multipart/form-data>
			<div class="modal-body" style="padding-left: 0; padding-top: 0; padding-bottom: 0">
				<input type="hidden" name="id" class="form-control" value="<?php echo $product->id;?>">
				<input type="hidden" name="old_image" class="form-control" value="<?php echo $product->image;?>">
				<div class="form-group bmd-form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="material-icons">all_inbox</i></div>
						</div>
						<input type="text" name="name" class="form-control" value="<?php echo $product->name;?>">
					</div>
				</div>
				<div class="form-group bmd-form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="material-icons">money</i></div>
						</div>
						<input type="text" name="price" class="form-control" value="<?php echo $product->price;?>">
					</div>
				</div>
				<div class="form-group bmd-form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="material-icons">production_quantity_limits</i></div>
						</div>
						<input type="text" name="qty" class="form-control" value="<?php echo $product->qty;?>">
					</div>
				</div>
				<div class="form-group bmd-form-group">
					<center>
					<img src="<?php if ($product->image){echo base_url('images/product/'.$product->image);}else{echo base_url('images/product/default.png');}?>" width="100px">
					</center>
				</div>
				<div class="input-group">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">image</i></div>
					</div>
					<input type="file" name="image" class="form-control" placeholder="Image">
				</div>
			</div>
	  		<div class="modal-footer">
			<select class="form-control" name="umkm_id">
			<?php 
			foreach($umkms as $umkm):?>
			  	<option value="<?php echo $umkm->id_umkm;?>" <?php if ($umkm->id_umkm == $product->umkm_id){ echo "selected";}?>><?php echo $umkm->nama_umkm;?></option>
			<?php endforeach; ?>
		  	</select>
				<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
				<button type="submit" class="btn btn-primary">Save changes</button>
	  		</div>
			</form>
		</div>
  	</div>
</div>
<?php endforeach;?>
<!-- Plugin for the momentJs  -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script> -->

<script type="text/javascript">
$(document).ready(function() {
    $('#Tableumkm').DataTable({
    	// "paging":   false,
        "ordering": false,
        "searching": false,
    });
} );
</script>