 <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"> -->
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
 
<!-- https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css -->
<div class="content" style="padding-top: 0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-6 col-sm-6">
				<div class="card">
					<div class="card-header card-header-success text-center">
						<h3 class="card-title">Data User</h3>
					</div>
					<div class="card-body"> 
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addData">
						Tambah Data
						</button>
						<table class="table table-stipped" id="TableUser">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th class="text-right">Status</th>
									<th class="text-right">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$no = 1;
								foreach($users as $user):?>
								<tr>
									<td class="text-center"><?php echo $no; $no++?></td>
									<td><?php echo $user->name;?></td>
									<td><?php echo $user->email;?></td>
									<td><?php echo $user->phone;?></td>
									<td class="text-right"><?php echo $user->state;?></td>
									<td class="td-actions text-right">
										<button type="button" rel="tooltip" class="btn btn-success btn-round" data-toggle="modal" data-target="#edit<?php echo $user->id;?>">
											<i class="material-icons">edit</i>
										</button>
										<button type="button" rel="tooltip" class="btn btn-danger btn-round" data-toggle="modal" data-target="#delete<?php echo $user->id;?>">
											<i class="material-icons">close</i>
										</button>
									</td>
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php if($this->session->flashdata('error')):?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert" style="position: absolute; right: 3%; bottom: 2%">
				<?php echo $this->session->flashdata('error');?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif ?>	
			<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-success alert-dismissible fade show" role="alert" style="position: absolute; right: 3%; bottom: 2%">
				<?php echo $this->session->flashdata('success');?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php endif ?>	 
		</div>
	</div>
</div>



<div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Tambah Data User</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body" style="padding-left: 0; padding-top: 0; padding-bottom: 0">
		 <form class="form" method="post" action="<?php echo base_url()?>index.php/admin/User/do_add">
			<div class="form-group bmd-form-group">
					<div class="input-group">
					  <div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">face</i></div>
					  </div>
					  <input type="text" name="name" class="form-control" placeholder="Name...">
					</div>
				</div>

				<div class="form-group bmd-form-group">
					<div class="input-group">
					  <div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">email</i></div>
					  </div>
					  <input type="email" name="email" class="form-control" placeholder="Email...">
					</div>
				</div>

				<div class="form-group bmd-form-group">
					<div class="input-group">
					  <div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">lock_outline</i></div>
					  </div>
					  <input type="password" name="password" placeholder="Password..." class="form-control">
					</div>
				</div>

				<div class="form-group bmd-form-group">
					<div class="input-group">
					  <div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">phone</i></div>
					  </div>
					  <input type="text" name="phone" placeholder="Phone..." class="form-control">
					</div>
				</div>
				
				<div class="form-group bmd-form-group">
					<div class="input-group">
					  <div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">place</i></div>
					  </div>
					  <textarea class="form-control" name="address" placeholder="Alamat..."></textarea>
					</div>
				</div>
		
	  </div>
	  <div class="modal-footer">
			<select class="form-control" name="state">
			<option value="#" selected="">Pilih Status</option>
		  	<option value="owner">Owner</option>
		  	<option value="user">User</option>
		  	</select>
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save changes</button>
		</form>
	  </div>
	</div>
  </div>
</div>
<?php foreach($users as $user):?>
<div class="modal fade" id="delete<?php echo $user->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-body">
		Are you sure want to delete <?php echo $user->name;?>?
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<a href="<?php echo base_url('index.php/admin/User/delete/'.$user->id.'')?>" class="btn btn-primary">Confirm</a>
	  </div>
	</div>
  </div>
</div>
<?php endforeach;?>


<?php foreach($users as $user):?>
<div class="modal fade" id="edit<?php echo $user->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Edit Data User</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body" style="padding-left: 0; padding-top: 0; padding-bottom: 0">
		 <form class="form" method="post" action="<?php echo base_url()?>index.php/admin/User/do_edit">
		 	<input type="hidden" name="id" class="form-control" value="<?php echo $user->id;?>">
			<div class="form-group bmd-form-group">
					<div class="input-group">
					  <div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">face</i></div>
					  </div>
					  <input type="text" name="name" class="form-control" value="<?php echo $user->name;?>">
					</div>
				</div>

				<div class="form-group bmd-form-group">
					<div class="input-group">
					  <div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">email</i></div>
					  </div>
					  <input type="email" name="email" class="form-control" value="<?php echo $user->email;?>">
					</div>
				</div>

				<div class="form-group bmd-form-group">
					<div class="input-group">
					  <div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">phone</i></div>
					  </div>
					  <input type="text" name="phone" class="form-control" value="<?php echo $user->phone;?>">
					</div>
				</div>
				
				<div class="form-group bmd-form-group">
					<div class="input-group">
					  <div class="input-group-prepend">
						<div class="input-group-text"><i class="material-icons">place</i></div>
					  </div>
					  <textarea class="form-control" name="address"><?php echo $user->address;?></textarea>
					</div>
				</div>
		
	  </div>
	  <div class="modal-footer">
			<select class="form-control" name="state">
		  	<option value="owner" <?php if($user->state == 'owner'){ echo "selected";}?>>Owner</option>
		  	<option value="user" <?php if($user->state == 'user'){ echo "selected";}?>>User</option>
		  	</select>
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save changes</button>
		</form>
	  </div>
	</div>
  </div>
</div>
<?php endforeach;?>
<!-- Plugin for the momentJs  -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script> -->

<script type="text/javascript">
$(document).ready(function() {
    $('#TableUser').DataTable({
    	// "paging":   false,
        "ordering": false,
        "searching": false,
    });
} );
</script>