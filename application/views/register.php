<section class="banner-area	">
		<!-- Content -->
	<div class="container">
		<div class="row  align-items-center">
			<center></center>
			<div class="col-lg-3"></div>
			<div class="col-lg-6 text-center text-lg-left">
				<h1 style="padding-bottom: 5%">Registrasi Pelanggan</h1>
				<form>
					<div class="card" style="margin-top:5%;margin-bottom: 10%">
						<div class="card-header">
							<center><h3>Registrasi Pelanggan</h3></center>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label for="exampleInputEmail1">Email address</label>
								<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Password</label>
								<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" aria-describedby="passwordHelp">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Nama</label>
								<input type="text" class="form-control" placeholder="Nama sesuai KTP">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Telepon</label>
								<input type="text" class="form-control" placeholder="No. Telp/ Hp">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Alamat</label>
								<textarea class="form-control" name="alamat">
									
								</textarea>
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
							<div class="form-group">
								Sudah punya akun? <a href="<?php echo base_url()?>index.php/Auth">Login</a>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-lg-3"></div>
		</div> <!-- / .row -->
	</div> <!-- / .container -->
</section>
<script src="<?php echo base_url();?>assets/rappo/plugins/jquery/jquery.min.js"></script>