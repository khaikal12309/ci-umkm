<section class="section bg-grey" id="feature" style="padding-top:3%">
	<div class="container">
		<div class="row justy-content-center">
			<div class="col-lg-12 col-sm-6 col-md-6" style="margin-top:3%">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link <?php if ($manage == 'laporan'){ echo"active";}?>" aria-current="page" href="<?php echo base_url();?>index.php/user/Home/manage_umkm/<?php echo $umkm->id_umkm;?>">Data Laporan</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?php if ($manage == 'product'){ echo"active";}?>" href="<?php echo base_url();?>index.php/user/Home/manage_product/<?php echo $umkm->id_umkm;?>">Data Product</a>
					</li>
				</ul>
			</div>
			<?php if ($manage=='laporan'){?>
			<div class="col-lg-12 col-sm-6 col-md-6" style="margin-top:3%">
				<div class="text-center feature-block">
					<div class="card">
						<div class="card-header">
							<div style="float:left">
								<button class="btn btn-primary btn-xm" data-toggle="modal" data-target="#addUMKM">Tambah Data</button> 
							</div>
							<h3><?php echo $umkm->nama_umkm; ?></h3>
							<div style="float:right">
								<?php if ($umkm->is_active){?>
									<span class="badge badge-success">Aktif</span>
								<?php } else {?>
									<span class="badge badge-dange">Nonaktif</span>
								<?php }?> 
							</div>
						</div>
						<div class="card-body">
							<table class="table table-stipped">
								<tr>
									<th>No</th>
									<th>Pemasukan</th>
									<th>Pengeluaran</th>
									<th>Tanggal</th>
								</tr>
								<?php
								$no = 1; 
								foreach($laporans as $laporan): ?>
									<tr>
										<td>1</td>
										<td><?php echo $laporan->pemasukan;?></td>
										<td><?php echo $laporan->pengeluaran;?></td>
										<td><?php echo $laporan->create_date;?></td>
									</tr> 
								<?php
									$no ++; 
									endforeach;?>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?php } else if ($manage == 'product'){?>
				<div class="col-lg-12" style="margin-top:3%">
				<button class="btn btn-primary btn-xm" data-toggle="modal" data-target="#addProduct">Tambah Product</button>
				</div>
				<?php foreach($products as $product):?>
					
					<div class="col-lg-4 col-sm-6 col-md-6" style="margin-top:3%">
						<div class="text-center feature-block">
							<div class="card">
								<div class="card-body">
									<img src="<?php if ($product->image){echo base_url('images/product/'.$product->image);}else{echo base_url('images/product/default.png');}?>" style="max-width:200px;height:200px">
								</div>
								<div class="card-footer">
									<b><?php echo $product->name;?></b><br/>
									<b><?php echo $product->price;?></b><br/>
									<b><?php echo $product->qty;?></b><br/>
									<b><?php echo $product->nama_umkm;?></b>

								</div>
							</div>
						</div>
					</div>
				<?php endforeach;?>
			<?php }?>
		</div>
	</div> <!-- / .container -->
</section>
<!-- Modal -->
<div class="modal fade" id="addUMKM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>index.php/user/Home/addPenghasilan">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Tambah Data Penghasilan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">RP</span>
						</div>
						<input type="text" class="form-control" placeholder="Pemasukan" name="pemasukan"/>
					</div>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">RP</span>
						</div>
						<input type="text" class="form-control" placeholder="Pengeluaran" name="pengeluaran"/>
					</div>
					<input type="hidden" class="form-control" name="umkm_id" value="<?php echo $umkm->id_umkm;?>"/>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>index.php/user/Home/addProduct" enctype="multipart/form-data">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Tambah Data Product</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="Nama Produk" name="name"/>
					</div>
					<div class="input-group mb-3">
						<input type="file" name="image"/>
					</div>
					<div class="input-group mb-3">
						<input type="number" class="form-control" placeholder="QTY" name="qty"/>
					</div>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">RP</span>
						</div>
						<input type="text" class="form-control" placeholder="Harga" name="price"/>
					</div>
					<input type="hidden" class="form-control" name="umkm_id" value="<?php echo $umkm->id_umkm;?>"/>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="<?php echo base_url();?>assets/rappo/plugins/jquery/jquery.min.js"></script>