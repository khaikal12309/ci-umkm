<section class="section bg-grey" id="feature" style="padding-top:3%">
	<div class="container">
		<div class="row justy-content-center">
			<?php foreach($products as $product):?>
				
				<div class="col-lg-4 col-sm-6 col-md-6" style="margin-top:3%">
					<div class="text-center feature-block">
						<div class="card">
							<div class="card-body">
								<img src="<?php if ($product->image){echo base_url('images/product/'.$product->image);}else{echo base_url('images/product/default.png');}?>" style="max-width:200px;height:200px">
							</div>
							<div class="card-footer">
								<b><?php echo $product->name;?></b><br/>
								<b><?php echo $product->price;?></b><br/>
								<b><?php echo $product->qty;?></b><br/>
								<b><?php echo $product->nama_umkm;?></b>

							</div>
						</div>
					</div>
				</div>
			<?php endforeach;?>
		</div>
	</div> <!-- / .container -->
</section>
<script src="<?php echo base_url();?>assets/rappo/plugins/jquery/jquery.min.js"></script>

<div class="modal fade" id="addUMKM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>index.php/user/Home/addPenghasilan">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Tambah Data UMKM</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="Nama UMKM" name="name"/>
					</div>
					<div class="input-group mb-3">
						<select class="form-control" name="kategori_id">
							<option selected="" value="#">Pilih Kategori UMKM</option>
						</select>
					</div>
					<div class="input-group mb-3">
						<select class="form-control" name="kecamatan_id">
							<option selected="" value="#">Kecamatan</option>
						</select>
					</div>
					<div class="input-group mb-3">
						<select class="form-control" name="desa_id">
							<option selected="" value="#">Desa</option>
						</select>
					</div>
					<input type="hidden" class="form-control" name="umkm_id" value="<?php echo $umkm->id_umkm;?>"/>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>