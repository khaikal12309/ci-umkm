<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <h2>Selamat Datang ! </h2><br/>
                    <h3>Anda login sebagai User</h3>
                </div>
            </div>
           
            <!-- ./col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>