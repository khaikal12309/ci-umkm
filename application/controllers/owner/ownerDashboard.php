<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ownerDashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		// $this->load->model('M_Notification');
		if($this->session->userdata('state') != "owner")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
    }
    
	public function index()
	{
		$data_to_template = array(
			'title' => 'Dashboard',
			'content' => $this->load->view('owner/dashboard', null, TRUE),
			'navbar' => $this->load->view('admin/layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('admin/layouts/sidebar', null, TRUE),
			'footer' => $this->load->view('admin/layouts/footer', null, TRUE),
		);

		// print_r($data_to_template['title']);
		// die();

		$this->load->view('admin/layouts/template', $data_to_template);	
	}
}
