<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		//load model admin
		$this->load->model('M_User');
		// if (! $this->session->userdata()){
		// 	$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
		// 	redirect("auth");
		// }
		// if($this->session->userdata('state') == "siswa")
		// {
		// 	redirect("siswa/SiswaDashboard");
		// } elseif ($this->session->userdata('state') == "guru") {
		// 	redirect("guru/GuruDashboard");
		// } elseif ($this->session->userdata('state') == "admin") {
		// 	redirect("admin/AdminDashboard");
		// } else{
		// 	$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
		// 	redirect("auth");
		// }
	}
	public function index()
	{	
		$data['page']= 'Login';
		$data_to_template = array(
			'page' => 'Login',
			'content' => $this->load->view('login',$data, TRUE),
			'header' => $this->load->view('front_layouts/header', $data, TRUE),
			'footer' => $this->load->view('front_layouts/footer', null, TRUE),
		);


		$this->load->view('front_layouts/template', $data_to_template);	
	}

	public function register()
	{	
		$data['page']= 'Register';
		$data_to_template = array(
			'page' => 'Login',
			'content' => $this->load->view('register',$data, TRUE),
			'header' => $this->load->view('front_layouts/header', $data, TRUE),
			'footer' => $this->load->view('front_layouts/footer', null, TRUE),
		);


		$this->load->view('front_layouts/template', $data_to_template);	
	}

	public function do_login()
	{
		// Tampung data
		$data['email'] = $this->input->post('email');
		$data['password'] = MD5($this->input->post('password'));

		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('login');
		} else {
			// Cek login
			$checking = $this->M_User->check_login($data);			
			if($checking->num_rows() > 0)
			{
				$data = $checking->row();
				$session_data = array(
					'id'   => $data->id,
					'name' => $data->name,
					'email' => $data->email,
					'state' => $data->state,
				);

				//set session userdata
				$this->session->set_userdata($session_data);
				if ($data->is_active){
					if ($data->state == 'admin') 
					{
						redirect('admin/adminDashboard');
					}
					if ($data->state == 'owner') 
					{
						redirect('admin/adminDashboard');
					}
					if ($data->state == 'user') 
					{
						$this->session->set_flashdata('success', 'Selamat datang '.$data->name.'');
						redirect('Home');
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'Akun anda tidak aktif, silahkan hubungi admin');
					$referred_from = $this->session->userdata('referred_from'); 
					redirect($referred_from, 'refresh');
				}
			}else{
				$this->session->set_flashdata('error', 'Username atau Password salah');
				$referred_from = $this->session->userdata('referred_from'); 
				redirect($referred_from, 'refresh');
			}
		}
		
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('Home'); 
	}

	
}