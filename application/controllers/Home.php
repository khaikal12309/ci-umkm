<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_Umkm');
		$this->load->model('M_Product');
		$this->load->model('M_Kategori');
		$this->load->model('M_Location');
	}

	public function index()
	{	
		$data['page'] = 'home';
		$data_to_template = array(
			'page' => 'dashboard',
			'content' => $this->load->view('home',null, TRUE),
			'header' => $this->load->view('front_layouts/header', $data, TRUE),
			'footer' => $this->load->view('front_layouts/footer', null, TRUE),
		);
		$this->load->view('front_layouts/template', $data_to_template);	
	}

	public function umkm()
	{	
		$data['page'] = 'umkm';
		$data['umkms'] = $this->M_Umkm->getAll();
		$data['kecamatans'] = $this->M_Location->getKecamatan();
		$data['desas'] = $this->M_Location->getDesa();
		$data_to_template = array(
			'page' => 'dashboard',
			'content' => $this->load->view('umkm',$data, TRUE),
			'header' => $this->load->view('front_layouts/header', $data, TRUE),
			'footer' => $this->load->view('front_layouts/footer', null, TRUE),
		);


		$this->load->view('front_layouts/template', $data_to_template);	
	}

	public function products()
	{	
		$data['page'] = 'products';
		$data['products'] = $this->M_Product->getAll();
		$data_to_template = array(
			'page' => 'produucts',
			'content' => $this->load->view('products',$data, TRUE),
			'header' => $this->load->view('front_layouts/header', $data, TRUE),
			'footer' => $this->load->view('front_layouts/footer', null, TRUE),
		);


		$this->load->view('front_layouts/template', $data_to_template);	
	}

	public function myumkm()
	{	
		$data['page'] = 'myumkm';
		$data['umkms'] = $this->M_Umkm->getAll();
		$data['kategoris'] = $this->M_Kategori->getAll();
		$data['kecamatans'] = $this->M_Location->getKecamatan();
		$data['desas'] = $this->M_Location->getDesa();
		$data_to_template = array(
			'page' => 'dashboard',
			'content' => $this->load->view('umkm',$data, TRUE),
			'header' => $this->load->view('front_layouts/header', $data, TRUE),
			'footer' => $this->load->view('front_layouts/footer', null, TRUE),
		);


		$this->load->view('front_layouts/template', $data_to_template);	
	}

	function getDesa(){
		$data = $this->M_Location->getDesaByKecamatan();
        echo json_encode($data);
    }

}
