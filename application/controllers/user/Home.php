<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_Umkm');
		$this->load->model('M_User');
		$this->load->model('M_Monitoring');
		$this->load->model('M_Product');
		$this->load->model('M_Notifikasi');
		if($this->session->userdata('state') != "user")
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("auth");
		}
    }
    
	public function index()
	{
		
	}

	public function profile()
	{	
		$data['page'] = '';
		$data['user'] = $this->M_User->getMe()[0];
		$data_to_template = array(
			'page' => 'profile',
			'content' => $this->load->view('profile',$data, TRUE),
			'header' => $this->load->view('front_layouts/header', $data, TRUE),
			'footer' => $this->load->view('front_layouts/footer', null, TRUE),
		);


		$this->load->view('front_layouts/template', $data_to_template);	
	}

	public function manage_umkm($id_umkm)
	{
		$data['page'] = 'myumkm';
		$data['manage'] = 'laporan';
		$data['umkm'] = $this->M_Umkm->getById($id_umkm)[0];
		$data['laporans'] = $this->M_Monitoring->getByUmkm($id_umkm);
		// print_r($data['umkm'][0]->id_umkm);
		// die();
		$data_to_template = array(
			'page' => 'dashboard',
			'content' => $this->load->view('manage_umkm',$data, TRUE),
			'header' => $this->load->view('front_layouts/header', $data, TRUE),
			'footer' => $this->load->view('front_layouts/footer', null, TRUE),
		);


		$this->load->view('front_layouts/template', $data_to_template);	
	}

	public function manage_product($id_umkm)
	{
		$data['page'] = 'myumkm';
		$data['manage'] = 'product';
		$data['umkm'] = $this->M_Umkm->getById($id_umkm)[0];
		$data['products'] = $this->M_Product->getByUmkm($id_umkm);
		// print_r($data['umkm'][0]->id_umkm);
		// die();
		$data_to_template = array(
			'page' => 'dashboard',
			'content' => $this->load->view('manage_umkm',$data, TRUE),
			'header' => $this->load->view('front_layouts/header', $data, TRUE),
			'footer' => $this->load->view('front_layouts/footer', null, TRUE),
		);


		$this->load->view('front_layouts/template', $data_to_template);	
	}

	public function addProduct()
	{
		$addProduct = $this->M_Product->save();
		if ($addProduct) {
			$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
		}else{
			$this->session->set_flashdata('error', 'Data gagal ditambahkan');
		}
		redirect ('user/Home/manage_product/'.$_POST['umkm_id']);
	}

	public function addPenghasilan(){
		$addPenghasilan = $this->M_Monitoring->save();

		if ($addPenghasilan) {
			$this->session->set_flashdata('success', 'Data berhasil diubah');
		}else{
			$this->session->set_flashdata('error', 'Data gagal diubah');
		}

		redirect ('user/Home/manage_umkm/'.$_POST['umkm_id']);
	}

	public function addUmkm()
	{
		$addUmkm = $this->M_Umkm->save();

		if ($addUmkm) {
			$umkm = $this->db->query('select * from tb_umkm where name = "'.$_POST['name'].'"')->result();
			$notif = $this->M_Notifikasi->save($umkm[0]->id);
			$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
		}else{
			$this->session->set_flashdata('error', 'Data gagal ditambahkan');
		}
		redirect ('/Home/myumkm/');
	}

	public function delete_umkm($id_umkm)
	{
		$delete = $this->M_Umkm->delete($id_umkm);
		if ($delete){
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		}else{
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		}
		redirect ('/Home/myumkm/');
	}
}
