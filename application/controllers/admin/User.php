<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_User');
		if($this->session->userdata('state') == Null)
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("Home");
		}
    }
    
	public function index()
	{
		$data['users']= $this->M_User->getAll();
		$data['page']="user";
		$data_to_template = array(
			'content' => $this->load->view('admin/user',$data, TRUE),
			'navbar' => $this->load->view('layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('layouts/sidebar', $data, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);

		// print_r($data_to_template['title']);
		// die();

		$this->load->view('layouts/template', $data_to_template);	
	}
	public function do_add()
	{
		
		$addUser = $this->M_User->save();
		if ($addUser) {
			$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
		}else{
			$this->session->set_flashdata('error', 'Data gagal ditambahkan');
		}

		
		redirect ('admin/User');

	}

	public function do_edit()
	{
		$edit = $this->M_User->update();
		
		if ($edit) {
			$this->session->set_flashdata('success', 'Data berhasil diubah');
		}else{
			$this->session->set_flashdata('error', 'Data gagal diubah');
		}

		redirect ('admin/User');	
	}

	public function delete($id)
	{
		$delete = $this->M_User->delete($id);
		if ($delete){
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		}else{
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		}
		redirect ('admin/User');
	}

	public function confirm($id_kegiatan)
	{

		$confirm = $this->M_Kegiatan->confirm($id_kegiatan);
		if ($confirm){
			$this->session->set_flashdata('success', 'Berita berhasil diconfirm');
		}else{
			$this->session->set_flashdata('error', 'Berita gagal diconfirm');
		}
		redirect ('admin/Kegiatan');
	}
}
