<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_Monitoring');
		$this->load->model('M_Configuration');
		$this->load->model('M_Umkm');
		$this->load->model('M_User');
		if($this->session->userdata('state') == Null)
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("Home");
		}
    }
    
	public function index()
	{
		$data['configs']= $this->M_Configuration->getAll();
		// $data['users'] = $this->M_User->getUser();
		$data['page']="config";
		$data_to_template = array(
			'content' => $this->load->view('admin/configuration',$data, TRUE),
			'navbar' => $this->load->view('layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('layouts/sidebar', $data, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);

		// print_r($data_to_template['title']);
		// die();

		$this->load->view('layouts/template', $data_to_template);	
	}

	public function do_add_periode_laporan()
	{
		$data['code']="PER-LAP";
		$data['name']="PERIODE LAPORAN";
		$addConfig = $this->M_Configuration->save($data);
		if ($addConfig) {
			$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
		}else{
			$this->session->set_flashdata('error', 'Data gagal ditambahkan');
		}

		
		redirect ('admin/Configuration');

	}

	public function run_cron()
	{
		$laporans = $this->M_Monitoring->getByDate();
		$config = $this->M_Configuration->getPeriod();
		if ($laporans){
			foreach($laporans as $laporan){
				$this->M_Umkm->NonActive($laporan->umkm_id);
			}
			$data['msg'] = "Terdapat ".count($laporan)." UMKM yang dinonaktifkan";
		}else{
			$data['msg'] = "Semua UMKM telah melaporkan penghasilan";
		}
		echo json_encode($data);
	}
}
