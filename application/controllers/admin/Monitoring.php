<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_Monitoring');
		$this->load->model('M_Umkm');
		$this->load->model('M_User');
		if($this->session->userdata('state') == Null)
		{
			$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
			redirect("Home");
		}
    }
    
	public function index()
	{
		$data['monitorings']= $this->M_Monitoring->getAll();
		$data['users'] = $this->M_User->getUser();
		$data['page']="monitoring";
		$data_to_template = array(
			'content' => $this->load->view('admin/monitoring',$data, TRUE),
			'navbar' => $this->load->view('layouts/navbar', null, TRUE),
			'sidebar' => $this->load->view('layouts/sidebar', $data, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);

		// print_r($data_to_template['title']);
		// die();

		$this->load->view('layouts/template', $data_to_template);	
	}
}
